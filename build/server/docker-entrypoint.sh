#!/bin/bash
set -e
cp -R /tmp/authorized_keys /root/.ssh
chown -R root:root /root/.ssh
exec "$@"
