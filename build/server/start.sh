#!/bin/bash
  
# turn on bash's job control
set -m
  
# Start openssh-server
./sshd_process &
  
# Start nginx
./nginx_process &

./update_process 

fg %1
