#!/bin/bash
  
# turn on bash's job control
set -m
  
# Start sys
./init_process &
  
# Start logs yum
./yumlog_process 


fg %1
