## How to use!
1. Run crate_env.sh to create work environment
```sh
bash crate_env.sh
```
---
2. Put you rpm packages in ./repo/centos/7/korneenkov/x86_64/
---
3. Put you public key in authorized_keys
---
```sh
cat .id_rsa.pub > ./.ssh/authorized_keys
```
4. install docker-compose if you need
---
```sh 
sh install-docker.sh
```
5. Run bundle 
---
```sh
docker-compose up -d 
```
6. show logs docker-compose bundle
---
```sh
docker-compose logs -f 
```

